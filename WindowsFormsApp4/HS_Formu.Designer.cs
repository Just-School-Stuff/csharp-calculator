﻿namespace WindowsFormsApp4
{
    partial class hesapMakinesi
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(hesapMakinesi));
            this.gecmisKutusu = new System.Windows.Forms.ListBox();
            this.sekmeler = new System.Windows.Forms.TabControl();
            this.hesaplamaSekmesi = new System.Windows.Forms.TabPage();
            this.silButonu = new System.Windows.Forms.Button();
            this.tasiButonu = new System.Windows.Forms.Button();
            this.Resimler = new System.Windows.Forms.ImageList(this.components);
            this.bilgiButonu = new System.Windows.Forms.Button();
            this.gizleButonu = new System.Windows.Forms.Button();
            this.cikis = new System.Windows.Forms.Button();
            this.arayuzButonu = new System.Windows.Forms.Button();
            this.gecmisButonu = new System.Windows.Forms.Button();
            this.islemKutusu = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.hepsiniTemizle = new System.Windows.Forms.Button();
            this.toplama = new System.Windows.Forms.Button();
            this.sonucKutusu = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.isaret = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.virgul = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.carpma = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.esittir = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.bolme = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.cikarma = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.gecmisSekmesi = new System.Windows.Forms.TabPage();
            this.gecmisKontrol = new System.Windows.Forms.Label();
            this.gecmisiTemizle = new System.Windows.Forms.Button();
            this.geriButonu = new System.Windows.Forms.Button();
            this.sekmeler.SuspendLayout();
            this.hesaplamaSekmesi.SuspendLayout();
            this.gecmisSekmesi.SuspendLayout();
            this.SuspendLayout();
            // 
            // gecmisKutusu
            // 
            this.gecmisKutusu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.gecmisKutusu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.gecmisKutusu.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold);
            this.gecmisKutusu.FormatString = "N0";
            this.gecmisKutusu.FormattingEnabled = true;
            this.gecmisKutusu.HorizontalScrollbar = true;
            this.gecmisKutusu.ItemHeight = 19;
            this.gecmisKutusu.Location = new System.Drawing.Point(3, 78);
            this.gecmisKutusu.Name = "gecmisKutusu";
            this.gecmisKutusu.Size = new System.Drawing.Size(343, 399);
            this.gecmisKutusu.TabIndex = 25;
            this.gecmisKutusu.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.gecmisKutusu_MouseDoubleClick);
            // 
            // sekmeler
            // 
            this.sekmeler.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.sekmeler.Controls.Add(this.hesaplamaSekmesi);
            this.sekmeler.Controls.Add(this.gecmisSekmesi);
            this.sekmeler.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold);
            this.sekmeler.Location = new System.Drawing.Point(-24, -4);
            this.sekmeler.Multiline = true;
            this.sekmeler.Name = "sekmeler";
            this.sekmeler.SelectedIndex = 0;
            this.sekmeler.Size = new System.Drawing.Size(377, 491);
            this.sekmeler.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.sekmeler.TabIndex = 28;
            // 
            // hesaplamaSekmesi
            // 
            this.hesaplamaSekmesi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.hesaplamaSekmesi.Controls.Add(this.silButonu);
            this.hesaplamaSekmesi.Controls.Add(this.tasiButonu);
            this.hesaplamaSekmesi.Controls.Add(this.bilgiButonu);
            this.hesaplamaSekmesi.Controls.Add(this.gizleButonu);
            this.hesaplamaSekmesi.Controls.Add(this.cikis);
            this.hesaplamaSekmesi.Controls.Add(this.arayuzButonu);
            this.hesaplamaSekmesi.Controls.Add(this.gecmisButonu);
            this.hesaplamaSekmesi.Controls.Add(this.islemKutusu);
            this.hesaplamaSekmesi.Controls.Add(this.button1);
            this.hesaplamaSekmesi.Controls.Add(this.hepsiniTemizle);
            this.hesaplamaSekmesi.Controls.Add(this.toplama);
            this.hesaplamaSekmesi.Controls.Add(this.sonucKutusu);
            this.hesaplamaSekmesi.Controls.Add(this.button2);
            this.hesaplamaSekmesi.Controls.Add(this.isaret);
            this.hesaplamaSekmesi.Controls.Add(this.button3);
            this.hesaplamaSekmesi.Controls.Add(this.virgul);
            this.hesaplamaSekmesi.Controls.Add(this.button4);
            this.hesaplamaSekmesi.Controls.Add(this.carpma);
            this.hesaplamaSekmesi.Controls.Add(this.button5);
            this.hesaplamaSekmesi.Controls.Add(this.esittir);
            this.hesaplamaSekmesi.Controls.Add(this.button6);
            this.hesaplamaSekmesi.Controls.Add(this.bolme);
            this.hesaplamaSekmesi.Controls.Add(this.button7);
            this.hesaplamaSekmesi.Controls.Add(this.cikarma);
            this.hesaplamaSekmesi.Controls.Add(this.button8);
            this.hesaplamaSekmesi.Controls.Add(this.button0);
            this.hesaplamaSekmesi.Controls.Add(this.button9);
            this.hesaplamaSekmesi.Location = new System.Drawing.Point(24, 4);
            this.hesaplamaSekmesi.Name = "hesaplamaSekmesi";
            this.hesaplamaSekmesi.Padding = new System.Windows.Forms.Padding(3);
            this.hesaplamaSekmesi.Size = new System.Drawing.Size(349, 483);
            this.hesaplamaSekmesi.TabIndex = 0;
            this.hesaplamaSekmesi.Text = "Hesaplama";
            // 
            // silButonu
            // 
            this.silButonu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.silButonu.AutoSize = true;
            this.silButonu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.silButonu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.silButonu.Cursor = System.Windows.Forms.Cursors.Default;
            this.silButonu.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.silButonu.FlatAppearance.BorderSize = 0;
            this.silButonu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.silButonu.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.silButonu.ForeColor = System.Drawing.Color.White;
            this.silButonu.Location = new System.Drawing.Point(92, 222);
            this.silButonu.MinimumSize = new System.Drawing.Size(78, 45);
            this.silButonu.Name = "silButonu";
            this.silButonu.Size = new System.Drawing.Size(78, 45);
            this.silButonu.TabIndex = 33;
            this.silButonu.Text = "<<";
            this.silButonu.UseVisualStyleBackColor = false;
            this.silButonu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.silButonu_MouseClick);
            this.silButonu.MouseEnter += new System.EventHandler(this.silButonu_MouseEnter);
            this.silButonu.MouseLeave += new System.EventHandler(this.silButonu_MouseLeave);
            // 
            // tasiButonu
            // 
            this.tasiButonu.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tasiButonu.AutoSize = true;
            this.tasiButonu.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tasiButonu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.tasiButonu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tasiButonu.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.tasiButonu.FlatAppearance.BorderSize = 0;
            this.tasiButonu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tasiButonu.ImageIndex = 3;
            this.tasiButonu.Location = new System.Drawing.Point(0, 0);
            this.tasiButonu.MinimumSize = new System.Drawing.Size(349, 15);
            this.tasiButonu.Name = "tasiButonu";
            this.tasiButonu.Size = new System.Drawing.Size(349, 15);
            this.tasiButonu.TabIndex = 32;
            this.tasiButonu.UseVisualStyleBackColor = true;
            this.tasiButonu.MouseUp += new System.Windows.Forms.MouseEventHandler(this.tasiButonu_MouseUp);
            // 
            // Resimler
            // 
            this.Resimler.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Resimler.ImageStream")));
            this.Resimler.TransparentColor = System.Drawing.Color.Transparent;
            this.Resimler.Images.SetKeyName(0, "PowerButton.png");
            this.Resimler.Images.SetKeyName(1, "visibility.png");
            this.Resimler.Images.SetKeyName(2, "Info_Simple_bw.svg.png");
            this.Resimler.Images.SetKeyName(3, "move_button.png");
            // 
            // bilgiButonu
            // 
            this.bilgiButonu.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.bilgiButonu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.bilgiButonu.FlatAppearance.BorderSize = 0;
            this.bilgiButonu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bilgiButonu.ImageIndex = 2;
            this.bilgiButonu.ImageList = this.Resimler;
            this.bilgiButonu.Location = new System.Drawing.Point(10, 16);
            this.bilgiButonu.Name = "bilgiButonu";
            this.bilgiButonu.Size = new System.Drawing.Size(42, 44);
            this.bilgiButonu.TabIndex = 31;
            this.bilgiButonu.UseVisualStyleBackColor = true;
            this.bilgiButonu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bilgiButonu_MouseClick);
            // 
            // gizleButonu
            // 
            this.gizleButonu.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.gizleButonu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.gizleButonu.FlatAppearance.BorderSize = 0;
            this.gizleButonu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gizleButonu.ImageIndex = 1;
            this.gizleButonu.ImageList = this.Resimler;
            this.gizleButonu.Location = new System.Drawing.Point(248, 16);
            this.gizleButonu.Name = "gizleButonu";
            this.gizleButonu.Size = new System.Drawing.Size(42, 44);
            this.gizleButonu.TabIndex = 30;
            this.gizleButonu.UseVisualStyleBackColor = true;
            this.gizleButonu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gizleButonu_MouseClick);
            // 
            // cikis
            // 
            this.cikis.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cikis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.cikis.FlatAppearance.BorderSize = 0;
            this.cikis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cikis.ImageIndex = 0;
            this.cikis.ImageList = this.Resimler;
            this.cikis.Location = new System.Drawing.Point(296, 16);
            this.cikis.Name = "cikis";
            this.cikis.Size = new System.Drawing.Size(42, 44);
            this.cikis.TabIndex = 29;
            this.cikis.UseVisualStyleBackColor = true;
            this.cikis.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cikisButonu_MouseClick);
            // 
            // arayuzButonu
            // 
            this.arayuzButonu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.arayuzButonu.AutoSize = true;
            this.arayuzButonu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.arayuzButonu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.arayuzButonu.Cursor = System.Windows.Forms.Cursors.Default;
            this.arayuzButonu.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.arayuzButonu.FlatAppearance.BorderSize = 0;
            this.arayuzButonu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.arayuzButonu.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.arayuzButonu.ForeColor = System.Drawing.Color.White;
            this.arayuzButonu.Location = new System.Drawing.Point(260, 66);
            this.arayuzButonu.MinimumSize = new System.Drawing.Size(78, 45);
            this.arayuzButonu.Name = "arayuzButonu";
            this.arayuzButonu.Size = new System.Drawing.Size(78, 45);
            this.arayuzButonu.TabIndex = 28;
            this.arayuzButonu.Text = "Koyu";
            this.arayuzButonu.UseVisualStyleBackColor = false;
            this.arayuzButonu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.arayuzButonu_MouseClick);
            // 
            // gecmisButonu
            // 
            this.gecmisButonu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gecmisButonu.AutoSize = true;
            this.gecmisButonu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.gecmisButonu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gecmisButonu.Cursor = System.Windows.Forms.Cursors.Default;
            this.gecmisButonu.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.gecmisButonu.FlatAppearance.BorderSize = 0;
            this.gecmisButonu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gecmisButonu.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.gecmisButonu.ForeColor = System.Drawing.Color.White;
            this.gecmisButonu.Location = new System.Drawing.Point(10, 66);
            this.gecmisButonu.MinimumSize = new System.Drawing.Size(78, 45);
            this.gecmisButonu.Name = "gecmisButonu";
            this.gecmisButonu.Size = new System.Drawing.Size(78, 45);
            this.gecmisButonu.TabIndex = 27;
            this.gecmisButonu.Text = "G";
            this.gecmisButonu.UseVisualStyleBackColor = false;
            this.gecmisButonu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gecmisButonu_MouseClick);
            // 
            // islemKutusu
            // 
            this.islemKutusu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.islemKutusu.BackColor = System.Drawing.Color.Transparent;
            this.islemKutusu.Font = new System.Drawing.Font("Arial", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.islemKutusu.Location = new System.Drawing.Point(10, 114);
            this.islemKutusu.MaximumSize = new System.Drawing.Size(328, 35);
            this.islemKutusu.Name = "islemKutusu";
            this.islemKutusu.Size = new System.Drawing.Size(328, 29);
            this.islemKutusu.TabIndex = 24;
            this.islemKutusu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.AutoSize = true;
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Default;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(10, 376);
            this.button1.MinimumSize = new System.Drawing.Size(78, 45);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(78, 45);
            this.button1.TabIndex = 0;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button1_MouseClick);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // hepsiniTemizle
            // 
            this.hepsiniTemizle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.hepsiniTemizle.AutoSize = true;
            this.hepsiniTemizle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.hepsiniTemizle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.hepsiniTemizle.Cursor = System.Windows.Forms.Cursors.Default;
            this.hepsiniTemizle.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.hepsiniTemizle.FlatAppearance.BorderSize = 0;
            this.hepsiniTemizle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hepsiniTemizle.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.hepsiniTemizle.ForeColor = System.Drawing.Color.White;
            this.hepsiniTemizle.Location = new System.Drawing.Point(10, 222);
            this.hepsiniTemizle.MinimumSize = new System.Drawing.Size(78, 45);
            this.hepsiniTemizle.Name = "hepsiniTemizle";
            this.hepsiniTemizle.Size = new System.Drawing.Size(78, 45);
            this.hepsiniTemizle.TabIndex = 22;
            this.hepsiniTemizle.Text = "AC";
            this.hepsiniTemizle.UseVisualStyleBackColor = false;
            this.hepsiniTemizle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.hepsiniTemizle_MouseClick);
            this.hepsiniTemizle.MouseEnter += new System.EventHandler(this.hepsiniTemizle_MouseEnter);
            this.hepsiniTemizle.MouseLeave += new System.EventHandler(this.hepsiniTemizle_MouseLeave);
            // 
            // toplama
            // 
            this.toplama.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.toplama.AutoSize = true;
            this.toplama.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.toplama.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.toplama.Cursor = System.Windows.Forms.Cursors.Default;
            this.toplama.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.toplama.FlatAppearance.BorderSize = 0;
            this.toplama.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.toplama.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.toplama.ForeColor = System.Drawing.Color.White;
            this.toplama.Location = new System.Drawing.Point(260, 222);
            this.toplama.MinimumSize = new System.Drawing.Size(78, 45);
            this.toplama.Name = "toplama";
            this.toplama.Size = new System.Drawing.Size(78, 45);
            this.toplama.TabIndex = 2;
            this.toplama.Text = "+";
            this.toplama.UseVisualStyleBackColor = false;
            this.toplama.MouseClick += new System.Windows.Forms.MouseEventHandler(this.topla_MouseClick);
            this.toplama.MouseEnter += new System.EventHandler(this.topla_MouseEnter);
            this.toplama.MouseLeave += new System.EventHandler(this.topla_MouseLeave);
            // 
            // sonucKutusu
            // 
            this.sonucKutusu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.sonucKutusu.BackColor = System.Drawing.Color.Transparent;
            this.sonucKutusu.Font = new System.Drawing.Font("Arial", 24F, System.Drawing.FontStyle.Bold);
            this.sonucKutusu.Location = new System.Drawing.Point(10, 149);
            this.sonucKutusu.Name = "sonucKutusu";
            this.sonucKutusu.Size = new System.Drawing.Size(328, 62);
            this.sonucKutusu.TabIndex = 21;
            this.sonucKutusu.Text = "0";
            this.sonucKutusu.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button2.AutoSize = true;
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button2.Cursor = System.Windows.Forms.Cursors.Default;
            this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(92, 376);
            this.button2.MinimumSize = new System.Drawing.Size(78, 45);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(78, 45);
            this.button2.TabIndex = 5;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button2_MouseClick);
            this.button2.MouseEnter += new System.EventHandler(this.button2_MouseEnter);
            this.button2.MouseLeave += new System.EventHandler(this.button2_MouseLeave);
            // 
            // isaret
            // 
            this.isaret.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.isaret.AutoSize = true;
            this.isaret.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.isaret.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.isaret.Cursor = System.Windows.Forms.Cursors.Default;
            this.isaret.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.isaret.FlatAppearance.BorderSize = 0;
            this.isaret.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.isaret.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.isaret.ForeColor = System.Drawing.Color.White;
            this.isaret.Location = new System.Drawing.Point(10, 428);
            this.isaret.MinimumSize = new System.Drawing.Size(78, 45);
            this.isaret.Name = "isaret";
            this.isaret.Size = new System.Drawing.Size(78, 45);
            this.isaret.TabIndex = 19;
            this.isaret.Text = "±";
            this.isaret.UseVisualStyleBackColor = false;
            this.isaret.MouseClick += new System.Windows.Forms.MouseEventHandler(this.isaret_MouseClick);
            this.isaret.MouseEnter += new System.EventHandler(this.isaret_MouseEnter);
            this.isaret.MouseLeave += new System.EventHandler(this.isaret_MouseLeave);
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button3.AutoSize = true;
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button3.Cursor = System.Windows.Forms.Cursors.Default;
            this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(176, 375);
            this.button3.MinimumSize = new System.Drawing.Size(78, 45);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(78, 45);
            this.button3.TabIndex = 6;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button3_MouseClick);
            this.button3.MouseEnter += new System.EventHandler(this.button3_MouseEnter);
            this.button3.MouseLeave += new System.EventHandler(this.button3_MouseLeave);
            // 
            // virgul
            // 
            this.virgul.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.virgul.AutoSize = true;
            this.virgul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.virgul.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.virgul.Cursor = System.Windows.Forms.Cursors.Default;
            this.virgul.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.virgul.FlatAppearance.BorderSize = 0;
            this.virgul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.virgul.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.virgul.ForeColor = System.Drawing.Color.White;
            this.virgul.Location = new System.Drawing.Point(176, 427);
            this.virgul.MinimumSize = new System.Drawing.Size(78, 45);
            this.virgul.Name = "virgul";
            this.virgul.Size = new System.Drawing.Size(78, 45);
            this.virgul.TabIndex = 18;
            this.virgul.Text = ",";
            this.virgul.UseVisualStyleBackColor = false;
            this.virgul.MouseClick += new System.Windows.Forms.MouseEventHandler(this.virgul_MouseClick);
            this.virgul.MouseEnter += new System.EventHandler(this.virgul_MouseEnter);
            this.virgul.MouseLeave += new System.EventHandler(this.virgul_MouseLeave);
            // 
            // button4
            // 
            this.button4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button4.AutoSize = true;
            this.button4.BackColor = System.Drawing.Color.Black;
            this.button4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button4.Cursor = System.Windows.Forms.Cursors.Default;
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(10, 326);
            this.button4.MinimumSize = new System.Drawing.Size(78, 45);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(78, 45);
            this.button4.TabIndex = 7;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button4_MouseClick);
            this.button4.MouseEnter += new System.EventHandler(this.button4_MouseEnter);
            this.button4.MouseLeave += new System.EventHandler(this.button4_MouseLeave);
            // 
            // carpma
            // 
            this.carpma.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.carpma.AutoSize = true;
            this.carpma.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.carpma.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.carpma.Cursor = System.Windows.Forms.Cursors.Default;
            this.carpma.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.carpma.FlatAppearance.BorderSize = 0;
            this.carpma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.carpma.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.carpma.ForeColor = System.Drawing.Color.White;
            this.carpma.Location = new System.Drawing.Point(260, 325);
            this.carpma.MinimumSize = new System.Drawing.Size(78, 45);
            this.carpma.Name = "carpma";
            this.carpma.Size = new System.Drawing.Size(78, 45);
            this.carpma.TabIndex = 17;
            this.carpma.Text = "x";
            this.carpma.UseVisualStyleBackColor = false;
            this.carpma.MouseClick += new System.Windows.Forms.MouseEventHandler(this.carpma_MouseClick);
            this.carpma.MouseEnter += new System.EventHandler(this.carpma_MouseEnter);
            this.carpma.MouseLeave += new System.EventHandler(this.carpma_MouseLeave);
            // 
            // button5
            // 
            this.button5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button5.AutoSize = true;
            this.button5.BackColor = System.Drawing.Color.Black;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button5.Cursor = System.Windows.Forms.Cursors.Default;
            this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(92, 326);
            this.button5.MinimumSize = new System.Drawing.Size(78, 45);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(78, 45);
            this.button5.TabIndex = 8;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button5_MouseClick);
            this.button5.MouseEnter += new System.EventHandler(this.button5_MouseEnter);
            this.button5.MouseLeave += new System.EventHandler(this.button5_MouseLeave);
            // 
            // esittir
            // 
            this.esittir.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.esittir.AutoSize = true;
            this.esittir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.esittir.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.esittir.Cursor = System.Windows.Forms.Cursors.Default;
            this.esittir.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.esittir.FlatAppearance.BorderSize = 0;
            this.esittir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.esittir.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.esittir.ForeColor = System.Drawing.Color.White;
            this.esittir.Location = new System.Drawing.Point(260, 428);
            this.esittir.MinimumSize = new System.Drawing.Size(78, 45);
            this.esittir.Name = "esittir";
            this.esittir.Size = new System.Drawing.Size(78, 45);
            this.esittir.TabIndex = 16;
            this.esittir.Text = "=";
            this.esittir.UseVisualStyleBackColor = false;
            this.esittir.MouseClick += new System.Windows.Forms.MouseEventHandler(this.esittir_MouseClick);
            this.esittir.MouseEnter += new System.EventHandler(this.esittir_MouseEnter);
            this.esittir.MouseLeave += new System.EventHandler(this.esittir_MouseLeave);
            // 
            // button6
            // 
            this.button6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button6.AutoSize = true;
            this.button6.BackColor = System.Drawing.Color.Black;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button6.Cursor = System.Windows.Forms.Cursors.Default;
            this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(176, 325);
            this.button6.MinimumSize = new System.Drawing.Size(78, 45);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(78, 45);
            this.button6.TabIndex = 9;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button6_MouseClick);
            this.button6.MouseEnter += new System.EventHandler(this.button6_MouseEnter);
            this.button6.MouseLeave += new System.EventHandler(this.button6_MouseLeave);
            // 
            // bolme
            // 
            this.bolme.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bolme.AutoSize = true;
            this.bolme.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.bolme.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.bolme.Cursor = System.Windows.Forms.Cursors.Default;
            this.bolme.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.bolme.FlatAppearance.BorderSize = 0;
            this.bolme.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bolme.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.bolme.ForeColor = System.Drawing.Color.White;
            this.bolme.Location = new System.Drawing.Point(260, 376);
            this.bolme.MinimumSize = new System.Drawing.Size(78, 45);
            this.bolme.Name = "bolme";
            this.bolme.Size = new System.Drawing.Size(78, 45);
            this.bolme.TabIndex = 15;
            this.bolme.Text = "÷";
            this.bolme.UseVisualStyleBackColor = false;
            this.bolme.MouseClick += new System.Windows.Forms.MouseEventHandler(this.bolme_MouseClick);
            this.bolme.MouseEnter += new System.EventHandler(this.bolme_MouseEnter);
            this.bolme.MouseLeave += new System.EventHandler(this.bolme_MouseLeave);
            // 
            // button7
            // 
            this.button7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button7.AutoSize = true;
            this.button7.BackColor = System.Drawing.Color.Black;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button7.Cursor = System.Windows.Forms.Cursors.Default;
            this.button7.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(10, 275);
            this.button7.MinimumSize = new System.Drawing.Size(78, 45);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(78, 45);
            this.button7.TabIndex = 10;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button7_MouseClick);
            this.button7.MouseEnter += new System.EventHandler(this.button7_MouseEnter);
            this.button7.MouseLeave += new System.EventHandler(this.button7_MouseLeave);
            // 
            // cikarma
            // 
            this.cikarma.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cikarma.AutoSize = true;
            this.cikarma.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.cikarma.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cikarma.Cursor = System.Windows.Forms.Cursors.Default;
            this.cikarma.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.cikarma.FlatAppearance.BorderSize = 0;
            this.cikarma.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cikarma.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.cikarma.ForeColor = System.Drawing.Color.White;
            this.cikarma.Location = new System.Drawing.Point(260, 274);
            this.cikarma.MinimumSize = new System.Drawing.Size(78, 45);
            this.cikarma.Name = "cikarma";
            this.cikarma.Size = new System.Drawing.Size(78, 45);
            this.cikarma.TabIndex = 14;
            this.cikarma.Text = "-";
            this.cikarma.UseVisualStyleBackColor = false;
            this.cikarma.MouseClick += new System.Windows.Forms.MouseEventHandler(this.cikarma_MouseClick);
            this.cikarma.MouseEnter += new System.EventHandler(this.cikarma_MouseEnter);
            this.cikarma.MouseLeave += new System.EventHandler(this.cikarma_MouseLeave);
            // 
            // button8
            // 
            this.button8.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button8.AutoSize = true;
            this.button8.BackColor = System.Drawing.Color.Black;
            this.button8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button8.Cursor = System.Windows.Forms.Cursors.Default;
            this.button8.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(92, 275);
            this.button8.MinimumSize = new System.Drawing.Size(78, 45);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(78, 45);
            this.button8.TabIndex = 11;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button8_MouseClick);
            this.button8.MouseEnter += new System.EventHandler(this.button8_MouseEnter);
            this.button8.MouseLeave += new System.EventHandler(this.button8_MouseLeave);
            // 
            // button0
            // 
            this.button0.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button0.AutoSize = true;
            this.button0.BackColor = System.Drawing.Color.Black;
            this.button0.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button0.Cursor = System.Windows.Forms.Cursors.Default;
            this.button0.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button0.FlatAppearance.BorderSize = 0;
            this.button0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button0.ForeColor = System.Drawing.Color.White;
            this.button0.Location = new System.Drawing.Point(92, 428);
            this.button0.MinimumSize = new System.Drawing.Size(78, 45);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(78, 45);
            this.button0.TabIndex = 13;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button0_MouseClick);
            this.button0.MouseEnter += new System.EventHandler(this.button0_MouseEnter);
            this.button0.MouseLeave += new System.EventHandler(this.button0_MouseLeave);
            // 
            // button9
            // 
            this.button9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button9.AutoSize = true;
            this.button9.BackColor = System.Drawing.Color.Black;
            this.button9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button9.Cursor = System.Windows.Forms.Cursors.Default;
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(176, 274);
            this.button9.MinimumSize = new System.Drawing.Size(78, 45);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(78, 45);
            this.button9.TabIndex = 12;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.MouseClick += new System.Windows.Forms.MouseEventHandler(this.button9_MouseClick);
            this.button9.MouseEnter += new System.EventHandler(this.button9_MouseEnter);
            this.button9.MouseLeave += new System.EventHandler(this.button9_MouseLeave);
            // 
            // gecmisSekmesi
            // 
            this.gecmisSekmesi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(47)))), ((int)(((byte)(47)))));
            this.gecmisSekmesi.Controls.Add(this.gecmisKontrol);
            this.gecmisSekmesi.Controls.Add(this.gecmisiTemizle);
            this.gecmisSekmesi.Controls.Add(this.geriButonu);
            this.gecmisSekmesi.Controls.Add(this.gecmisKutusu);
            this.gecmisSekmesi.ForeColor = System.Drawing.Color.White;
            this.gecmisSekmesi.Location = new System.Drawing.Point(24, 4);
            this.gecmisSekmesi.Name = "gecmisSekmesi";
            this.gecmisSekmesi.Padding = new System.Windows.Forms.Padding(3);
            this.gecmisSekmesi.Size = new System.Drawing.Size(349, 483);
            this.gecmisSekmesi.TabIndex = 1;
            this.gecmisSekmesi.Text = "Geçmiş";
            // 
            // gecmisKontrol
            // 
            this.gecmisKontrol.AutoSize = true;
            this.gecmisKontrol.Location = new System.Drawing.Point(7, 78);
            this.gecmisKontrol.Name = "gecmisKontrol";
            this.gecmisKontrol.Size = new System.Drawing.Size(138, 16);
            this.gecmisKontrol.TabIndex = 30;
            this.gecmisKontrol.Text = "Henüz Geçmiş Yok";
            // 
            // gecmisiTemizle
            // 
            this.gecmisiTemizle.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.gecmisiTemizle.AutoSize = true;
            this.gecmisiTemizle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.gecmisiTemizle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.gecmisiTemizle.Cursor = System.Windows.Forms.Cursors.Default;
            this.gecmisiTemizle.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.gecmisiTemizle.FlatAppearance.BorderSize = 0;
            this.gecmisiTemizle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.gecmisiTemizle.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.gecmisiTemizle.ForeColor = System.Drawing.Color.White;
            this.gecmisiTemizle.Location = new System.Drawing.Point(94, 14);
            this.gecmisiTemizle.MinimumSize = new System.Drawing.Size(78, 45);
            this.gecmisiTemizle.Name = "gecmisiTemizle";
            this.gecmisiTemizle.Size = new System.Drawing.Size(78, 45);
            this.gecmisiTemizle.TabIndex = 29;
            this.gecmisiTemizle.Text = "AC";
            this.gecmisiTemizle.UseVisualStyleBackColor = false;
            this.gecmisiTemizle.MouseClick += new System.Windows.Forms.MouseEventHandler(this.gecmisiTemizle_MouseClick);
            this.gecmisiTemizle.MouseEnter += new System.EventHandler(this.gecmisiTemizle_MouseEnter);
            this.gecmisiTemizle.MouseLeave += new System.EventHandler(this.gecmisiTemizle_MouseLeave);
            // 
            // geriButonu
            // 
            this.geriButonu.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.geriButonu.AutoSize = true;
            this.geriButonu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(27)))), ((int)(((byte)(27)))), ((int)(((byte)(27)))));
            this.geriButonu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.geriButonu.Cursor = System.Windows.Forms.Cursors.Default;
            this.geriButonu.FlatAppearance.BorderColor = System.Drawing.Color.Green;
            this.geriButonu.FlatAppearance.BorderSize = 0;
            this.geriButonu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.geriButonu.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.geriButonu.ForeColor = System.Drawing.Color.White;
            this.geriButonu.Location = new System.Drawing.Point(10, 14);
            this.geriButonu.MinimumSize = new System.Drawing.Size(78, 45);
            this.geriButonu.Name = "geriButonu";
            this.geriButonu.Size = new System.Drawing.Size(78, 45);
            this.geriButonu.TabIndex = 28;
            this.geriButonu.Text = "Geri";
            this.geriButonu.UseVisualStyleBackColor = false;
            this.geriButonu.MouseClick += new System.Windows.Forms.MouseEventHandler(this.geriButonu_MouseClick);
            this.geriButonu.MouseEnter += new System.EventHandler(this.geriButonu_MouseEnter);
            this.geriButonu.MouseLeave += new System.EventHandler(this.geriButonu_MouseLeave);
            // 
            // hesapMakinesi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ClientSize = new System.Drawing.Size(348, 481);
            this.ControlBox = false;
            this.Controls.Add(this.sekmeler);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(348, 481);
            this.MinimumSize = new System.Drawing.Size(348, 481);
            this.Name = "hesapMakinesi";
            this.Padding = new System.Windows.Forms.Padding(10);
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.sekmeler.ResumeLayout(false);
            this.hesaplamaSekmesi.ResumeLayout(false);
            this.hesaplamaSekmesi.PerformLayout();
            this.gecmisSekmesi.ResumeLayout(false);
            this.gecmisSekmesi.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ListBox gecmisKutusu;
        private System.Windows.Forms.TabPage hesaplamaSekmesi;
        private System.Windows.Forms.TabPage gecmisSekmesi;
        private System.Windows.Forms.TabControl sekmeler;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button toplama;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button cikarma;
        private System.Windows.Forms.Button bolme;
        private System.Windows.Forms.Button esittir;
        private System.Windows.Forms.Button carpma;
        private System.Windows.Forms.Button virgul;
        private System.Windows.Forms.Button isaret;
        private System.Windows.Forms.Label sonucKutusu;
        private System.Windows.Forms.Button hepsiniTemizle;
        private System.Windows.Forms.Label islemKutusu;
        private System.Windows.Forms.Button gecmisButonu;
        private System.Windows.Forms.Button geriButonu;
        private System.Windows.Forms.Button gecmisiTemizle;
        private System.Windows.Forms.Button arayuzButonu;
        private System.Windows.Forms.Button cikis;
        private System.Windows.Forms.ImageList Resimler;
        private System.Windows.Forms.Button gizleButonu;
        private System.Windows.Forms.Button bilgiButonu;
        private System.Windows.Forms.Button tasiButonu;
        private System.Windows.Forms.Button silButonu;
        private System.Windows.Forms.Label gecmisKontrol;
    }
}

