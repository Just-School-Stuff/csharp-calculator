﻿namespace WindowsFormsApp4
{
    // Arac-Gerec
    static class AracGerec
    {
        // int islemleri
        static public int Topla(int d1, int d2) { return d1 + d2; }

        static public int Cikar(int d1, int d2) { return d1 - d2; }

        static public int Carp(int d1, int d2) { return d1 * d2; }

        static public int Bol(int d1, int d2) { return d1 / d2; }

        static public int isaretDegistir(int d) { return -1 * d; }

        // Long islemleri
        static public long Topla(long d1, long d2) { return d1 + d2; }

        static public long Cikar(long d1, long d2) { return d1 - d2; }

        static public long Carp(long d1, long d2) { return d1 * d2; }

        static public long Bol(long d1, long d2) { return d1 / d2; }

        static public long isaretDegistir(long d) { return -1 * d; }
    }
}