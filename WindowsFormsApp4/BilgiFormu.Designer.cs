﻿namespace HesapMakinesi
{
    partial class BilgiFormu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BilgiFormu));
            this.Resimler = new System.Windows.Forms.ImageList(this.components);
            this.bilgiKutusu = new System.Windows.Forms.Label();
            this.BilgiResmi = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.BilgiResmi)).BeginInit();
            this.SuspendLayout();
            // 
            // Resimler
            // 
            this.Resimler.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("Resimler.ImageStream")));
            this.Resimler.TransparentColor = System.Drawing.Color.Transparent;
            this.Resimler.Images.SetKeyName(0, "Info_Simple_bw.svg.png");
            // 
            // bilgiKutusu
            // 
            resources.ApplyResources(this.bilgiKutusu, "bilgiKutusu");
            this.bilgiKutusu.Name = "bilgiKutusu";
            // 
            // BilgiResmi
            // 
            this.BilgiResmi.BackColor = System.Drawing.Color.Transparent;
            resources.ApplyResources(this.BilgiResmi, "BilgiResmi");
            this.BilgiResmi.Name = "BilgiResmi";
            this.BilgiResmi.TabStop = false;
            // 
            // BilgiFormu
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(37)))));
            this.ControlBox = false;
            this.Controls.Add(this.BilgiResmi);
            this.Controls.Add(this.bilgiKutusu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BilgiFormu";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Deactivate += new System.EventHandler(this.BilgiFormu_Deactivate);
            this.Click += new System.EventHandler(this.BilgiFormu_Click);
            ((System.ComponentModel.ISupportInitialize)(this.BilgiResmi)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ImageList Resimler;
        private System.Windows.Forms.PictureBox BilgiResmi;
        internal System.Windows.Forms.Label bilgiKutusu;
    }
}