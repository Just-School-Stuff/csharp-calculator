﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using HesapMakinesi;

namespace WindowsFormsApp4
{
    public partial class hesapMakinesi : Form
    {
        public hesapMakinesi()
        {
            InitializeComponent();
        }
        int sonuc = 0;
        int eskiSonuc = 0;
        int degisken = 0;
        string eskiIslemKutusu = "";
        int basamak = 10;

        Color arayuzRengi = Color.FromArgb(47, 47, 47);
        Color sButonRengi = Color.Black;
        Color butonRengi = Color.FromArgb(27, 27, 27);
        Color degisimRegi = Color.Green;
        Color yaziRengi = Color.White;
        Color ozelRenk = Color.Green;
        Color hepsiniTemizleRengi = Color.Red;

        string sonIslem = "";

        bool esittirKontrol = false;
        

        //      ---     -------     ---
        //      ---     button0     ---
        //      ---     -------     ---
        private void button0_MouseEnter(object sender, EventArgs e)
        {
            button0.BackColor = butonRengi;
        }

        private void button0_MouseLeave(object sender, EventArgs e)
        {
            button0.BackColor = sButonRengi;
        }

        private void button0_MouseClick(object sender, MouseEventArgs e)
        {
            button0.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e); 

            degisken *= basamak;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button1     ---
        //      ---     -------     ---
        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.BackColor = butonRengi;
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.BackColor = sButonRengi;
        }

        private void button1_MouseClick(object sender, MouseEventArgs e)
        {
            button1.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken++;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button2     ---
        //      ---     -------     ---
        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.BackColor = butonRengi;
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.BackColor = sButonRengi;
        }

        private void button2_MouseClick(object sender, MouseEventArgs e)
        {
            button2.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 2;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button3     ---
        //      ---     -------     ---
        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.BackColor = butonRengi;
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.BackColor = sButonRengi;
        }

        private void button3_MouseClick(object sender, MouseEventArgs e)
        {
            button3.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 3;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button4     ---
        //      ---     -------     ---
        private void button4_MouseEnter(object sender, EventArgs e)
        {
            button4.BackColor = butonRengi;
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.BackColor = sButonRengi;
        }

        private void button4_MouseClick(object sender, MouseEventArgs e)
        {
            button4.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 4;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button5     ---
        //      ---     -------     ---
        private void button5_MouseEnter(object sender, EventArgs e)
        {
            button5.BackColor = butonRengi;
        }

        private void button5_MouseLeave(object sender, EventArgs e)
        {
            button5.BackColor = sButonRengi;
        }

        private void button5_MouseClick(object sender, MouseEventArgs e)
        {
            button5.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 5;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button6     ---
        //      ---     -------     ---
        private void button6_MouseEnter(object sender, EventArgs e)
        {
            button6.BackColor = butonRengi;
        }

        private void button6_MouseLeave(object sender, EventArgs e)
        {
            button6.BackColor = sButonRengi;
        }

        private void button6_MouseClick(object sender, MouseEventArgs e)
        {
            button6.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 6;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button7     ---
        //      ---     -------     ---
        private void button7_MouseEnter(object sender, EventArgs e)
        {
            button7.BackColor = butonRengi;
        }

        private void button7_MouseLeave(object sender, EventArgs e)
        {
            button7.BackColor = sButonRengi;
        }

        private void button7_MouseClick(object sender, MouseEventArgs e)
        {
            button7.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 7;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button8     ---
        //      ---     -------     ---
        private void button8_MouseEnter(object sender, EventArgs e)
        {
            button8.BackColor = butonRengi;
        }

        private void button8_MouseLeave(object sender, EventArgs e)
        {
            button8.BackColor = sButonRengi;
        }

        private void button8_MouseClick(object sender, MouseEventArgs e)
        {
            button8.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 8;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     button9     ---
        //      ---     -------     ---
        private void button9_MouseEnter(object sender, EventArgs e)
        {
            button9.BackColor = butonRengi;
        }

        private void button9_MouseLeave(object sender, EventArgs e)
        {
            button9.BackColor = sButonRengi;
        }

        private void button9_MouseClick(object sender, MouseEventArgs e)
        {
            button9.BackColor = System.Drawing.Color.FromArgb(67, 67, 67);
            if (esittirKontrol) hepsiniTemizle_MouseClick(sender, e);

            degisken *= basamak;
            degisken += 9;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     Toplama     ---
        //      ---     -------     ---
        private void topla_MouseEnter(object sender, EventArgs e)
        {
            toplama.BackColor = ozelRenk;
        }

        private void topla_MouseLeave(object sender, EventArgs e)
        {
            toplama.BackColor = butonRengi;
        }

        private void topla_MouseClick(object sender, MouseEventArgs e)
        {
            toplama.BackColor = ozelRenk;
            try
            {
                if(sonIslem.Count() != 0)
                {
                    if (!esittirKontrol)
                    {
                        if (sonIslem == "+") sonuc = AracGerec.Topla(sonuc, degisken);
                        if (sonIslem == "-") sonuc = AracGerec.Cikar(sonuc, degisken);
                        if (sonIslem == "*") sonuc = AracGerec.Carp(sonuc, degisken);
                        if (sonIslem == "÷") sonuc = AracGerec.Bol(sonuc, degisken);
                    }
                }
                else
                {
                    sonuc = degisken;
                }
                islemKutusu.Text = sonuc.ToString() + " + ";
                degisken = 0;
                sonucKutusu.Text = degisken.ToString();
                sonIslem = "+";
            }
            catch (ArgumentOutOfRangeException) { }
            catch (OutOfMemoryException) { }
            catch { }
            esittirKontrol = false;
        }

        //      ---     -------     ---
        //      ---     Cikarma     ---
        //      ---     -------     ---
        private void cikarma_MouseEnter(object sender, EventArgs e)
        {
            cikarma.BackColor = ozelRenk;
        }

        private void cikarma_MouseLeave(object sender, EventArgs e)
        {
            cikarma.BackColor = butonRengi;
        }

        private void cikarma_MouseClick(object sender, MouseEventArgs e)
        {
            cikarma.BackColor = ozelRenk;
            try
            {
                if (sonIslem.Count() != 0)
                {
                    if (!esittirKontrol)
                    {
                        if (sonIslem == "+") sonuc = AracGerec.Topla(sonuc, degisken);
                        if (sonIslem == "-") sonuc = AracGerec.Cikar(sonuc, degisken);
                        if (sonIslem == "*") sonuc = AracGerec.Carp(sonuc, degisken);
                        if (sonIslem == "÷") sonuc = AracGerec.Bol(sonuc, degisken);
                    }
                }
                else
                {
                    sonuc = degisken;
                }
                islemKutusu.Text = sonuc.ToString() + " - ";
                degisken = 0;
                sonucKutusu.Text = degisken.ToString();
                sonIslem = "-";
            }
            catch (ArgumentOutOfRangeException) { }
            catch (OutOfMemoryException) { }
            catch { }
            esittirKontrol = false;
        }

        //      ---     -------     ---
        //      ---     Carpma      ---
        //      ---     -------     ---
        private void carpma_MouseEnter(object sender, EventArgs e)
        {
            carpma.BackColor = ozelRenk;
        }

        private void carpma_MouseLeave(object sender, EventArgs e)
        {
            carpma.BackColor = butonRengi;
        }

        private void carpma_MouseClick(object sender, MouseEventArgs e)
        {
            carpma.BackColor = ozelRenk;
            try
            {
                if (sonIslem.Count() != 0)
                {
                    if (!esittirKontrol)
                    {
                        if (sonIslem == "+") sonuc = AracGerec.Topla(sonuc, degisken);
                        if (sonIslem == "-") sonuc = AracGerec.Cikar(sonuc, degisken);
                        if (sonIslem == "*") sonuc = AracGerec.Carp(sonuc, degisken);
                        if (sonIslem == "÷") sonuc = AracGerec.Bol(sonuc, degisken);
                    }
                }
                else
                {
                    sonuc = degisken;
                }
                islemKutusu.Text = sonuc.ToString() + " * ";
                degisken = 0;
                sonucKutusu.Text = degisken.ToString();
                sonIslem = "*";
            }
            catch (ArgumentOutOfRangeException) { }
            catch (OutOfMemoryException) { }
            catch { }
            esittirKontrol = false;
        }

        //      ---     -------     ---
        //      ---     Bolme       ---
        //      ---     -------     ---
        private void bolme_MouseEnter(object sender, EventArgs e)
        {
            bolme.BackColor = ozelRenk;
        }

        private void bolme_MouseLeave(object sender, EventArgs e)
        {
            bolme.BackColor = butonRengi;
        }

        private void bolme_MouseClick(object sender, MouseEventArgs e)
        {
            bolme.BackColor = ozelRenk;
            try
            {
                if (sonIslem.Count() != 0)
                {
                    if (!esittirKontrol)
                    {
                        if (sonIslem == "+") sonuc = AracGerec.Topla(sonuc, degisken);
                        if (sonIslem == "-") sonuc = AracGerec.Cikar(sonuc, degisken);
                        if (sonIslem == "*") sonuc = AracGerec.Carp(sonuc, degisken);
                        if (sonIslem == "÷") sonuc = AracGerec.Bol(sonuc, degisken);
                    }
                }
                else
                {
                    sonuc = degisken;
                }
                islemKutusu.Text = sonuc.ToString() + " ÷ ";
                degisken = 0;
                sonucKutusu.Text = degisken.ToString();
                sonIslem = "÷";
            }
            catch(DivideByZeroException) { }
            catch (ArgumentOutOfRangeException) { }
            catch (OutOfMemoryException) { }
            catch { }
            esittirKontrol = false;
        }

        //      ---     -------     ---
        //      ---     Esittir     ---
        //      ---     -------     ---
        private void esittir_MouseEnter(object sender, EventArgs e)
        {
            esittir.BackColor = ozelRenk;
        }

        private void esittir_MouseLeave(object sender, EventArgs e)
        {
            esittir.BackColor = butonRengi;
        }

        private void esittir_MouseClick(object sender, MouseEventArgs e)
        {
            esittir.BackColor = ozelRenk;
            eskiSonuc = sonuc;
            Boolean K = true;
            if ( sonIslem != "" )
            {
                try
                {
                    if (sonIslem == "+") sonuc = AracGerec.Topla(sonuc, degisken);
                    if (sonIslem == "-") sonuc = AracGerec.Cikar(sonuc, degisken);
                    if (sonIslem == "*") sonuc = AracGerec.Carp(sonuc, degisken);
                    if (sonIslem == "÷") sonuc = AracGerec.Bol(sonuc, degisken);
                }
                catch (DivideByZeroException) { islemKutusu.Text = "Sonuç Tanımlanmadı! (0/0)"; K = false; }
                catch (ArgumentOutOfRangeException) { islemKutusu.Text = "Sınır Aşıldı!"; K = false; }
                catch (OutOfMemoryException) { islemKutusu.Text = "Hafıza Sınırı Aşıldı!"; K = false; }
                catch { }
            }
            if (eskiIslemKutusu == "")
            {
                if(K) islemKutusu.Text = islemKutusu.Text + degisken.ToString() + " = ";
                eskiIslemKutusu = islemKutusu.Text;
            }
            else
            {
                if (K) islemKutusu.Text = eskiSonuc.ToString() + " " + sonIslem.ToString() + " " + degisken.ToString() + " = ";
                eskiIslemKutusu = islemKutusu.Text;
            }
            if (K) gecmisKutusu.Items.Add(islemKutusu.Text + sonuc.ToString());
            if (K) sonucKutusu.Text = sonuc.ToString();
            esittirKontrol = true;
        }

        //      ---     -------     ---
        //      --- Hepsini Temizle ---
        //      ---     -------     ---
        private void hepsiniTemizle_MouseEnter(object sender, EventArgs e)
        {
            hepsiniTemizle.BackColor = hepsiniTemizleRengi;
        }

        private void hepsiniTemizle_MouseLeave(object sender, EventArgs e)
        {
            hepsiniTemizle.BackColor = butonRengi;
        }

        private void hepsiniTemizle_MouseClick(object sender, MouseEventArgs e)
        {
            hepsiniTemizle.BackColor = ozelRenk;
            

            degisken = 0;
            sonuc = 0;
            sonucKutusu.Text = degisken.ToString();
            eskiIslemKutusu = islemKutusu.Text  = sonIslem = "";
            esittirKontrol = false;
        }

        //      ---     -------     ---
        //      ---       Sil       ---
        //      ---     -------     ---
        private void silButonu_MouseEnter(object sender, EventArgs e)
        {
            silButonu.BackColor = hepsiniTemizleRengi;
        }

        private void silButonu_MouseLeave(object sender, EventArgs e)
        {
            silButonu.BackColor = butonRengi;
        }

        private void silButonu_MouseClick(object sender, MouseEventArgs e)
        {
            silButonu.BackColor = degisimRegi;
            degisken /= 10;
            sonucKutusu.Text = degisken.ToString();
        }

        //      ---     -------     ---
        //      ---     Gecmis      ---
        //      ---     -------     ---
        private void gecmisButonu_MouseClick(object sender, MouseEventArgs e)
        {
            if (gecmisKutusu.Items.Count != 0)
            {
                gecmisKontrol.Hide();
            }
            else
            {
                gecmisKontrol.Show();
            }
            sekmeler.SelectTab(gecmisSekmesi);
        }

        //      ---     -------     ---
        //      ---       Geri      ---
        //      ---     -------     ---
        private void geriButonu_MouseEnter(object sender, EventArgs e)
        {
            geriButonu.BackColor = butonRengi;
        }

        private void geriButonu_MouseLeave(object sender, EventArgs e)
        {
            geriButonu.BackColor = butonRengi;
        }

        private void geriButonu_MouseClick(object sender, MouseEventArgs e)
        {
            sekmeler.SelectTab(hesaplamaSekmesi);
        }

        //      ---     -------     ---
        //      --- Gecmisi Temizle ---
        //      ---     -------     ---
        private void gecmisiTemizle_MouseEnter(object sender, EventArgs e)
        {
            gecmisiTemizle.BackColor = hepsiniTemizleRengi;
        }

        private void gecmisiTemizle_MouseLeave(object sender, EventArgs e)
        {
            gecmisiTemizle.BackColor = butonRengi;
        }

        private void gecmisiTemizle_MouseClick(object sender, MouseEventArgs e)
        {
            gecmisiTemizle.BackColor = ozelRenk;
            gecmisKutusu.Items.Clear();
            gecmisKontrol.Show();
        }

        //      ---     -------     ---
        //      ---     Arayuz      ---
        //      ---     -------     ---
        private void arayuzButonu_MouseClick(object sender, MouseEventArgs e)
        {
            if(arayuzRengi == Color.FromArgb(47, 47, 47))
            {
                arayuzRengi = Color.WhiteSmoke;
                butonRengi = Color.DimGray;
                sButonRengi = Color.FromArgb(67, 67, 67);
                yaziRengi = Color.Black;
                arayuzButonu.Text = "Açık";
            }
            else
            {
                arayuzRengi = Color.FromArgb(47, 47, 47);
                butonRengi = Color.FromArgb(27, 27, 27);
                sButonRengi = Color.Black;
                yaziRengi = Color.White;
                arayuzButonu.Text = "Koyu";
            }

            //Arayuz Arkaplan
            hesaplamaSekmesi.BackColor = arayuzRengi;
            gecmisSekmesi.BackColor = arayuzRengi;
            gecmisKutusu.BackColor = arayuzRengi;

            //Arayuz Yazi
            hesaplamaSekmesi.ForeColor = yaziRengi;
            gecmisSekmesi.ForeColor = yaziRengi;
            gecmisKutusu.ForeColor = yaziRengi;

            // Buton Arkaplan
            arayuzButonu.BackColor = butonRengi;
            gecmisButonu.BackColor = butonRengi;
            gecmisiTemizle.BackColor = butonRengi;
            silButonu.BackColor = butonRengi;
            geriButonu.BackColor = butonRengi;
            button0.BackColor = sButonRengi;
            button1.BackColor = sButonRengi;
            button2.BackColor = sButonRengi;
            button3.BackColor = sButonRengi;
            button4.BackColor = sButonRengi;
            button5.BackColor = sButonRengi;
            button6.BackColor = sButonRengi;
            button7.BackColor = sButonRengi;
            button8.BackColor = sButonRengi;
            button9.BackColor = sButonRengi;
            hepsiniTemizle.BackColor = butonRengi;
            isaret.BackColor = butonRengi;
            virgul.BackColor = butonRengi;
            esittir.BackColor = butonRengi;
            toplama.BackColor = butonRengi;
            cikarma.BackColor = butonRengi;
            carpma.BackColor = butonRengi;
            bolme.BackColor = butonRengi;

            // Buton Yazi
            arayuzButonu.ForeColor = yaziRengi;
            gecmisButonu.ForeColor = yaziRengi;
            gecmisiTemizle.ForeColor = yaziRengi;
            silButonu.ForeColor = yaziRengi;
            geriButonu.ForeColor = yaziRengi;
            button0.ForeColor = yaziRengi;
            button1.ForeColor = yaziRengi;
            button2.ForeColor = yaziRengi;
            button3.ForeColor = yaziRengi;
            button4.ForeColor = yaziRengi;
            button5.ForeColor = yaziRengi;
            button6.ForeColor = yaziRengi;
            button7.ForeColor = yaziRengi;
            button8.ForeColor = yaziRengi;
            button9.ForeColor = yaziRengi;
            hepsiniTemizle.ForeColor = yaziRengi;
            isaret.ForeColor = yaziRengi;
            virgul.ForeColor = yaziRengi;
            esittir.ForeColor = yaziRengi;
            toplama.ForeColor = yaziRengi;
            cikarma.ForeColor = yaziRengi;
            carpma.ForeColor = yaziRengi;
            bolme.ForeColor = yaziRengi;
        }
        
        //      ---     -------     ---
        //      ---     Isaret      ---
        //      ---     -------     ---
        private void isaret_MouseClick(object sender, MouseEventArgs e)
        {
            if (esittirKontrol)
            {
                sonuc = AracGerec.isaretDegistir(sonuc);
            }
            else if( sonIslem.Count() != -1 && !esittirKontrol)
            {
                degisken = AracGerec.isaretDegistir(degisken);
            }
            else
            {
                degisken = AracGerec.isaretDegistir(degisken);
            }
            sonucKutusu.Text = (esittirKontrol) ? sonuc.ToString() : degisken.ToString();
        }

        private void isaret_MouseEnter(object sender, EventArgs e)
        {

        }

        private void isaret_MouseLeave(object sender, EventArgs e)
        {

        }

        //      ---     -------     ---
        //      ---     Virgul      ---
        //      ---     -------     ---
        private void virgul_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void virgul_MouseEnter(object sender, EventArgs e)
        {

        }

        private void virgul_MouseLeave(object sender, EventArgs e)
        {

        }

        //      ---     -------     ---
        //      ---      Cikis      ---
        //      ---     -------     ---
        private void cikisButonu_MouseClick(object sender, MouseEventArgs e)
        {
            Close();
        }

        //      ---     -------     ---
        //      ---      Gizle      ---
        //      ---     -------     ---
        private void gizleButonu_MouseClick(object sender, MouseEventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        //      ---     -------     ---
        //      ---      Tasi      ---
        //      ---     -------     ---
        private void tasiButonu_MouseUp(object sender, MouseEventArgs e)
        {
            Point tasimaNoktasi = MousePosition;
            tasimaNoktasi.X -= tasiButonu.Width / 2;
            tasimaNoktasi.Y -= tasiButonu.Height / 2;
			
            if (e.Button == MouseButtons.Left)
            {
                Location = tasimaNoktasi;
            }
        }

        //      ---     -------     ---
        //      ---      Bilgi      ---
        //      ---     -------     ---
        private void bilgiButonu_MouseClick(object sender, MouseEventArgs e)
        {
            BilgiFormu bilgiForm = new BilgiFormu();

            bilgiForm.BackColor = arayuzRengi;
            bilgiForm.ForeColor = yaziRengi;
            bilgiForm.bilgiKutusu.ForeColor = yaziRengi;

            bilgiForm.Show();
        }

        private void pencereTasi(MouseEventArgs e)
        {
            //while(OnMouseUp(e) == )
        }

        //      ---     -------     ---
        //      ---  Gecmis Kutusu  ---
        //      ---     -------     ---
        private void gecmisKutusu_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if(gecmisKutusu.SelectedIndex != -1)
            {
                string gecmisIslem = gecmisKutusu.Text; // Geçmiş işlemin yazısı

                string[] parca = gecmisIslem.Split(' ');

                string eskiIslemKutusu;

                int.TryParse(parca[0], out eskiSonuc);
                sonIslem = parca[1];
                int.TryParse(parca[2], out degisken);
                int.TryParse(parca[4], out sonuc);

                if(gecmisKutusu.SelectedIndex - 1 != -1)
                {
                    eskiIslemKutusu = gecmisKutusu.Items[gecmisKutusu.SelectedIndex - 1].ToString();

                    string[] eskiParca = eskiIslemKutusu.Split(' ');
                    //int.TryParse(eskiParca[0], out eskiSonuc);
                }
                else { eskiIslemKutusu = ""; }
                
                islemKutusu.Text = eskiSonuc.ToString() + " " + sonIslem + " " + degisken.ToString() + " = ";
                sonucKutusu.Text = sonuc.ToString();


                sekmeler.SelectTab(hesaplamaSekmesi);
            }
        }
    }
}